/*
 * Quinta fase da Olimpíada de robótica educativa livre
 * 
 * Código para controle do robô via bluetooth, utilizando smartphone/tablet
 * 
 */

// inclusão de bibliotecas que darão o suporte necessário para a execução do código 
#include <SoftwareSerial.h>
#include <Servo.h>

#define dir_frent 4
#define dir_atras 5
#define esq_frent 6
#define esq_atras 7

// definição dos ângulos do servo
#define angulo_atacar   170
#define angulo_recolher 10

Servo servo;  // declaração da variável que corresponderá ao servo motor

// inicializa RX e TX do bluetooth 
// OBS: a saída do bluetooth será a entrada do arduino
SoftwareSerial bt(10,11);

// Funções utilizadas pelo código abaixo
bool nenhumNovoComando();
void rencolheBracoSeTempo();
void atacaBracoSeTempo();
void setUltimoTempo(unsigned long long int ult);

// Definição das configurações iniciais
void setup()
{
    servo.attach(8);      // inicialização do servo motor no pino 8
    bt.begin(9600);       // inicialização do componente bluetooth
    Serial.begin(9600);   // inicialização das portas seriais
    pinMode(dir_frent, OUTPUT);
    pinMode(dir_atras, OUTPUT);
    pinMode(esq_frent, OUTPUT);
    pinMode(esq_atras, OUTPUT);
}

// Função onde está o código será executado continuamente
void loop()
{
    if( nenhumNovoComando() )
    {
        digitalWrite(dir_frent, LOW);
        digitalWrite(dir_atras, LOW);
        digitalWrite(esq_frent, LOW);
        digitalWrite(esq_atras, LOW);
    }
    rencolheBracoSeTempo();
    char ch;
    // if(Serial.available())    // se o Serial Monitor estiver disponível 
    if(bt.available())           // se o Bluetooth estiver disponível 
    {
        // ch = Serial.read();   // Ler caractere do Serial Monitor (arduino IDE)
        ch = bt.read();          // Ler caractere do Bluetooth
        setUltimoTempo(millis());
        switch(ch)               // "carrega" o valor da variável ch, se ela for igual a alguns dos casos abaixo elo codigo será executado
        {
        case 'B':
            atacaBracoSeTempo();
            break;
        case 'W':
            digitalWrite(dir_frent, HIGH);
            digitalWrite(esq_frent, HIGH);
            digitalWrite(esq_atras, LOW);
            digitalWrite(dir_atras, LOW);
            break;
        case 'S':
            digitalWrite(dir_frent, LOW);
            digitalWrite(esq_frent, LOW);
            digitalWrite(esq_atras, HIGH);
            digitalWrite(dir_atras, HIGH);
            break;
        case 'A':
            digitalWrite(dir_frent, HIGH);
            digitalWrite(esq_frent, LOW);
            digitalWrite(esq_atras, HIGH);
            digitalWrite(dir_atras, LOW);
            break;
        case 'D':
            digitalWrite(dir_frent, LOW);
            digitalWrite(esq_frent, HIGH);
            digitalWrite(esq_atras, LOW);
            digitalWrite(dir_atras, HIGH);
            break;
        default:
            setUltimoTempo(0);
        }
    }
    // Fim do Loop
}



// ==================== NÃO MODIFICAR ====================
bool ataque=false;
unsigned long long int ultimo=0, servot=0;
// Funções de controle da "arma"
bool nenhumNovoComando() {
    return millis()-ultimo > 150;
}
void rencolheBracoSeTempo() {
    if (ataque && millis()-servot > 1000) {
        servo.write(angulo_atacar);
        ataque = false;
    }
}
void atacaBracoSeTempo() {
    if(!ataque && millis()-servot > 3000) {
        ataque = true;
        servo.write(angulo_recolher);
        servot = millis();
    }
}
void setUltimoTempo(unsigned long long int ult) {
    ultimo = ult;
}
// ==================== NÃO MODIFICAR ====================

