/*
 * Quarta fase da Olimpíada de robótica educativa livre
 * 
 * Código para controle do robô via bluetooth, utilizando smartphone/tablet
 * 
 * Favor antes de modificar o este código entrar em contato com a organização da olimpíada (robo4educa@gmail.com)
 */

// inclusão de bibliotecas que darão o suporte necessário para a execução do código 
#include <SoftwareSerial.h>
#include <Servo.h>

#define dir_frent 4
#define dir_atras 5
#define esq_frent 6
#define esq_atras 7

#define angulo_atacar	170
#define angulo_recolher	10

Servo servo;  // declaração da variável que corresponderá ao servo motor
bool ataque=false; // variável booleana(valores somente true ou false) usada no controle do servo motor (se ele esta da na posição 0 ou 180)

unsigned long long int ultimo=0, servot=0;

// inicializa RX e TX do bluetooth 
// OBS: a saída do bluetooth será a entrada do arduino
SoftwareSerial bt(10,11);

// Definição das configurações iniciais
void setup()
{
	servo.attach(8);  // inicialização do servo motor no pino 8
	bt.begin(9600);   // inicialização do componente bluetooth
	Serial.begin(9600);   // inicialização das portas seriais
	pinMode(dir_frent, OUTPUT);
	pinMode(dir_atras, OUTPUT);
	pinMode(esq_frent, OUTPUT);
	pinMode(esq_atras, OUTPUT);
}

// Função onde está o código será executado continuamente
void loop()
{
	if(millis()-ultimo > 150)
	{
		digitalWrite(dir_frent, LOW);
		digitalWrite(dir_atras, LOW);
		digitalWrite(esq_frent, LOW);
		digitalWrite(esq_atras, LOW);
	}
	if(ataque && millis()-servot > 1000)
	{
		servo.write(angulo_atacar);
		ataque = false;
	}
	char ch;
	if(/*Serial*/bt.available())	// se o Bluetooth estiver disponível então 
	{
		ch = /*Serial*/bt.read();
		ultimo = millis();
		switch(ch)	// "carrega" o valor da variável ch, se ela for igual a alguns dos casos abaixo elo codigo será executado
		{
		case 'B':
			if(!ataque && millis()-servot > 3000)
			{
				ataque = true;
				servo.write(angulo_recolher);
				servot = millis();
			}
			break;
		case 'W':
			digitalWrite(dir_frent, HIGH);
			digitalWrite(esq_frent, HIGH);
			digitalWrite(esq_atras, LOW);
			digitalWrite(dir_atras, LOW);
			break;
		case 'S':
			digitalWrite(dir_frent, LOW);
			digitalWrite(esq_frent, LOW);
			digitalWrite(esq_atras, HIGH);
			digitalWrite(dir_atras, HIGH);
			break;
		case 'A':
			digitalWrite(dir_frent, HIGH);
			digitalWrite(esq_frent, LOW);
			digitalWrite(esq_atras, HIGH);
			digitalWrite(dir_atras, LOW);
			break;
		case 'D':
			digitalWrite(dir_frent, LOW);
			digitalWrite(esq_frent, HIGH);
			digitalWrite(esq_atras, LOW);
			digitalWrite(dir_atras, HIGH);
			break;
		default:
			ultimo = 0;
		}
	}
	//EndLoop
}
